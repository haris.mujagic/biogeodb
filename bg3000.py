from flaskApp import create_app
from flaskApp.database import db_session

app = create_app('defaultblabla')
@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()
