{pkgs}: pkgs.stdenv.mkDerivation rec {
    pname = "wgs84-lv03";
    version = "unknown";
    src = pkgs.fetchgit {
      url = "https://github.com/ValentinMinder/Swisstopo-WGS84-LV03";
      sha256 = "sha256-Zy9VwIelSaD8L8gCBXbDqi6dAsXQIKBWx+QbqYLhcDM=";
    };
    installPhase =
      ''
      mkdir -p $out/${pname}
      cp -v  $src/scripts/js/wgs84_ch1903.js $out/${pname}/
      '';
}
