let
  pkgs = import (builtins.fetchTarball {
    # Descriptive name to make the store path easier to identify
    name = "nixos-unstable-2021-10-20";
    # Commit hash for nixos-unstable as of 2018-09-12
    url = "https://github.com/NixOS/nixpkgs/archive/01eaa66bb663412c31b5399334f118030a91f1aa.tar.gz";
    # Hash obtained using `nix-prefetch-url --unpack <url>`
    sha256 = "00953iwfh5x3ckhifwjk87cyx3lgn456fi1apjsmxsjpzksn4rzb";
  }) {};
in
   pkgs
