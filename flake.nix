{
  description = "A very basic flake";
  inputs = {
    nixpkgs.url = github:NixOS/nixpkgs/nixos-22.05;
    flake-utils.url = "github:numtide/flake-utils";
    # leaflet = {
    #   type = "tarball";
    #   url = "https://leafletjs-cdn.s3.amazonaws.com/content/leaflet/v1.8.0/leaflet.zip";
    # };
    mach-nix = {
      url = "github:DavHau/mach-nix";
    };
    pypi-deps-db.url = "github:DavHau/pypi-deps-db";
  };
  outputs = { self, nixpkgs, flake-utils, mach-nix, pypi-deps-db }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        # pkgs = nixpkgs.legacyPackages.${system};
        pkgs = import nixpkgs { inherit system; };
        machNix = mach-nix.lib."${system}";
        devEnvironment = machNix.mkPython {
          requirements = builtins.readFile ./requirements/local.txt;
        };
        dbName = "bioGeoDB";
        llPkgs = import ./leaflet.nix {inherit pkgs;};
      in rec {
        biogeoregio = pkgs.fetchzip {
          url = "http://data.geo.admin.ch/ch.bafu.biogeographische_regionen/data.zip";
          name = "biogeoregio";
          stripRoot = false;
          hash = "sha256-XbWJ43XEJlFPuq0/mRqfLrZsLArI2zxUxa64sjqXxtE=";
        };
        packages.server = pkgs.stdenv.mkDerivation rec {
          name = "bioGeoDB";
          env = pkgs.buildEnv { name = name; paths = buildInputs; };
          builder = builtins.toFile "builder.sh" ''
            source $stdenv/setup; ln -s $env $out
          '';
          buildInputs = with pkgs.python39Packages; [
              devEnvironment
            ];
          version = "0.0.1";
        };
        packages.leaflet = llPkgs.leaflet;
        packages.llMarkerCluster = llPkgs.llMarkerCluster;
        packages.wgs84-lv03 = import ./wgs84-lv03.nix { inherit pkgs; };
        devShell = pkgs.mkShell {
          buildInputs = packages.server.buildInputs ++ [
            biogeoregio
            devEnvironment
            ( pkgs.postgresql.withPackages (p: [ p.postgis ]) )
            (pkgs.writeShellScriptBin "importTestData" ''
              python -c "import test; test.importAll()"
              '')
            (pkgs.writeShellScriptBin "initBgDB" ''
              initdb $PGDATA --auth=trust --locale en_US.UTF-8 >/dev/null
              pgStart
              createdb ${dbName}
              psql ${dbName} -c "CREATE EXTENSION postgis;"
              python -c "from flaskApp import database; database.init_db()"
              # srid 2056 LV03/LV95 -> 4326 wsg84
              shp2pgsql -s 2056:4326 -c -D -I ${biogeoregio.outPath}/BiogeographischeRegionen/N2020_Revision_BiogeoRegion.dbf public.bgregio | psql -d ${dbName}
              python importSpezies.py
              '')
            (pkgs.writeShellScriptBin "copystaticfiles" ''
              cp -ru static ./flaskApp/
              cp -r ${packages.wgs84-lv03.outPath}/* ./flaskApp/static/js/
              cp -r ${packages.leaflet.outPath}/* ./flaskApp/static/js/
              cp -r ${packages.llMarkerCluster.outPath}/* ./flaskApp/static/js/
              chmod -R u+w flaskApp/static/js
            '')
            (pkgs.writeShellScriptBin "runGuni" ''
                gunicorn "flaskApp:create_app('dev')"
            '')
            (pkgs.writeShellScriptBin "runFlask" ''
                # 'flask run' does not work, see:
                # https://github.com/NixOS/nixpkgs/issues/33093
                # https://github.com/NixOS/nixpkgs/issues/42924
                python -m flask run
            '')
            (pkgs.writeShellScriptBin "pgStop" ''
              pg_ctl stop
            '')
            (pkgs.writeShellScriptBin "pgStart" ''
              pg_ctl start -l $LOG_PATH -o "-c listen_addresses= -c unix_socket_directories=$PGHOST"
            '')
            pkgs.less
            pkgs.man
            pkgs.git
            ( pkgs.postgresql.withPackages (p: [ p.postgis ]) )
          ];
          # export PS1='🕷 \u@biogeo \$ '
          shellHook = ''
            source secrets.sh
            export PS1='\360\237\225\267 \u@biogeo \$ '
            export FLASK_APP=bg3000
            export FLASK_ENV=development
            export PGDATA=$PWD/postgres_data
            export PGHOST=$PWD/postgres
            export LOG_PATH=$PWD/postgres/LOG
            export PGDATABASE=postgres
            export DATABASE_URL=postgresql:///${dbName}?host=$PGHOST
            # stop server on exit
            # trap "pg_ctl stop" EXIT
            if [ ! -d $PGHOST ]; then
              mkdir -p $PGHOST
            fi
            if [ ! -d $PGDATA ]; then
              initBgDB
            fi
            echo "Checking postgres status..."
            pg_ctl status
            if [ $? -eq 3 ]; then
              pg_ctl start -l $LOG_PATH -o "-c listen_addresses= -c unix_socket_directories=$PGHOST"; return;
            fi
            '';
            };
            defaultPackage = packages.server;
        }
      );
}
