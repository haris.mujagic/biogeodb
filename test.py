import csv
import re
import datetime
from flaskApp.database import db_session
from flaskApp.fund.models import Spezies, Fund
from flaskApp.auth.model import Benutzende

userdata = [{
    'email': 'haris.mujagic@unibas.ch',
    'admin': False
},{
    'email': 'admin@unibas.ch',
    'admin': True
},{
    'email': 'durga.bahadur.singh@gmail.com',
    'admin': True
}]

funddata = [{
    'gattung': 'Atractides',
    'art': 'panniculatus',
    'legit': 'lucas',
    'ort': '8.03835 46.679697',
    'determinavit': 'lucas',
    'datumGefunden': '06.08.2015',
    'habitat': 'hypokrenal',
    'publication': 'mumuscience'

},{
    'gattung': 'Atractides',
    'art': 'vaginalis',
    'legit': 'lucas',
    'ort': '8.03835 46.679697',
    'determinavit': 'lucas',
    'datumGefunden': '06.08.2015',
    'habitat': 'hypokrenal',
    'publication': 'mumuscience'
},{
    'gattung': 'Atractides',
    'art': 'walteri',
    'legit': 'lucas',
    'ort': '8.03835 46.679697',
    'determinavit': 'lucas',
    'datumGefunden': '06.08.2015',
    'habitat': 'hypokrenal', 
    'publication': 'mumuscience'
},{
    'gattung': 'Partnunia',
    'art': 'steinmanni',
    'legit': 'lucas',
    'ort': '7.416143 46.681254',
    'determinavit': 'lucas',
    'datumGefunden': '06.08.2015',
    'habitat': 'eukrenal', 
    'publication': 'mumuscience'
},{
    'gattung': 'Sperchon',
    'art': 'longirostris',
    'legit': 'lucas',
    'ort': '7.416143 46.681254',
    'determinavit': 'lucas',
    'datumGefunden': '06.08.2015',
    'habitat': 'eukrenal', 
    'publication': 'mumuscience'
},{
    'gattung': 'Sperchon',
    'art': 'mutilus',
    'legit': 'lucas',
    'ort': '7.416143 46.681254',
    'determinavit': 'lucas',
    'datumGefunden': '06.08.2015',
    'habitat': 'eukrenal', 
    'publication': 'mumuscience'
},{
    'gattung': 'Sperchon',
    'art': 'thienemanni',
    'legit': 'lucas',
    'ort': '7.74951 47.22937',
    'determinavit': 'lucas',
    'datumGefunden': '20.09.2019',
    'habitat': 'eukrenal', 
    'publication': 'mumuscience'
}]

date_time_str = '06.08.2015'
date_time_obj = datetime.datetime.strptime(date_time_str, '%d.%m.%Y')


def importUser():
    for user in userdata:
        userObj = Benutzende(
            email = user['email'],
            admin = user['admin'],
            active_member = True
        )
        print(user)
        try:
            db_session.add(userObj)
            db_session.commit()
        except:
            db_session.rollback()
            pass

def importFund():
    for fund in funddata:
        print(fund)
        gefunden=datetime.datetime.strptime(fund['datumGefunden'], '%d.%m.%Y')
        determinavit=fund['determinavit']
        legit=fund['legit']
        spezies=db_session.query(Spezies).filter(
            Spezies.gattung==fund['gattung']
            ).filter(
                Spezies.art==fund['art']
            ).first()
        user = db_session.query(Benutzende).filter(
                Benutzende.email=='haris.mujagic@unibas.ch'
                ).first()
        fundObj = Fund(
            # ownerList=[user],
            ort='POINT({})'.format(fund['ort']),
            datumGefunden=gefunden,
            legit=legit,
            spezies=spezies,
            determinavit=determinavit,
            habitat=fund['habitat'],
            publication=fund['publication']
        )
        fundObj.ownerList.append(user)
        try:
            db_session.add(fundObj)
            db_session.commit()
        except Exception as e:
            print(e)
            db_session.rollback()
            pass

def importAll():
    print("importAll")
    importUser()
    importFund()
