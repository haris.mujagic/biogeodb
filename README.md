% bioGeoDB 

The biogeo 3000 app

# Install

First, install nix package manager.

then be sure to find yourself in the home dir of biogeodb.
    $ nix-shell --pure

now, you moved to the nix dev environment.

# Init db

In the nix dev shell, create biogeodb and then initialize it:

   biogeodb> createdb biogeodb
   biogeodb> CREATE EXTENSION postgis;
   $ python -m flask db init

# Run

   biogeodb> runFlask


Open http://127.0.0.1:5000 in a browser.


