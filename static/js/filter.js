const speziesSelect = document.querySelector('#spezies_id');
const speziesReset = document.querySelector('#resetspezies');
const legitSelect = document.querySelector('#legit_id');
const subsetSelect = document.querySelector('#filterspezies');
const habitatSelect = document.querySelector('#habitat_id');
const altitudeSelect = document.querySelector('#altitude_id')

speziesReset.addEventListener('click', (event) => {
  console.log(mediath)
  showFundList(mediath)
  legitSelect.value = ""
  speziesSelect.value = ""
  habitatSelect.value = ""
  altitudeSelect.value = ""
})

const specfil = f => {
  if (speziesSelect.value != ""){
    return speziesSelect.value == f.spezies.id
  }
  else return true
}
const legfil = fund => fund.legit.search(legitSelect.value) !== -1

const habifil = fund => fund.habitat.search(habitatSelect.value) !== -1

const altifil = fund => {
  if (altitudeSelect.value != ""){
    if (altitudeSelect.value == "low"){
      return fund.altitude <= 600 
    }
    else if (altitudeSelect.value == "mid"){
      return fund.altitude > 600 && fund.altitude <= 1800
    }
    else if(altitudeSelect.value == "high"){
      return fund.altitude > 1800
    }
  }
  else return true
}

const filterfunctions = [specfil, legfil, habifil, altifil]

subsetSelect.addEventListener('click', (event) => {
  const filtered = filterfunctions.reduce(
    (acc, f) => acc.filter(fund => f(fund)),
    mediath
  )
  showFundList(filtered)
})
