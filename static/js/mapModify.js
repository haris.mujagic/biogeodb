document.getElementById("longitude").addEventListener('input', (lng) => {
    if (lng.data === ".") return
    var newVal = +(lng.target.value)
    if (typeof newVal == 'number' && !isNaN(newVal)) setSingleMarker(
        {
            lat: currentMarker.getLatLng().lat,
            lng: newVal
        }
    )
});
document.getElementById("latitude").addEventListener('input', (lat) => {
    if (lat.data === ".") return
    var newVal = +(lat.target.value)
    
    if (typeof newVal == 'number' && !isNaN(newVal)) setSingleMarker(
        {
            lat: newVal,
            lng: currentMarker.getLatLng().lng
        }
    )
});
map.on('click', function(e){
    setSingleMarker(e.latlng)
    console.log(e.latlng)
    document.getElementById("longitude").value = e.latlng.lng
    document.getElementById("latitude").value = e.latlng.lat
});

//document.getElementById("bgregio")('input', (lat) => {

//})

var setSingleMarker = (latlng) => {
    if (typeof currentMarker !== 'undefined') map.removeLayer(currentMarker)
    currentMarker = new L.marker(latlng)
    map.addLayer(currentMarker)
    map.setView(new L.LatLng(latlng.lat, latlng.lng), 13);
    map.flyTo(latlng)
    document.getElementById("longitude").value = latlng.lng
    document.getElementById("latitude").value = latlng.lat
    const wgs = Swisstopo.WGStoCH(latlng.lat, latlng.lng)
    fetch(`https://api3.geo.admin.ch/rest/services/height?easting=${wgs[0]}&northing=${wgs[1]}`)
        .then(response => response.json())
        .then(data => {
            console.log(data)
            document.getElementById("altitude").value = data.height
        })       
    fetch(`https://api3.geo.admin.ch/rest/services/api/MapServer/identify?geometryType=esriGeometryPoint&geometry=${wgs[0]},${wgs[1]}&imageDisplay=0,0,0&mapExtent=0,0,0,0&tolerance=0&layers=all:ch.swisstopo.swissboundaries3d-gemeinde-flaeche.fill,ch.swisstopo-vd.ortschaftenverzeichnis_plz&returnGeometry=false`)
        .then(response => response.json())
        .then(data => {
            console.log(data)
            document.getElementById("zip").value = data.results[1].attributes.plz
            document.getElementById("municipality").value = data.results[0].attributes.gemname
            document.getElementById("canton").value = data.results[0].attributes.kanton
        })
    fetch(`/fund/bgregio?lat=${latlng.lat}&lng=${latlng.lng}`)
        .then(response => response.json())
        .then(data => {
            console.log(data)
            document.getElementById("bgregio").textContent = data
        })
    
}

