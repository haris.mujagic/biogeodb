var generateDefaultMap = () => {
    var saintlouis = { lat: 47.5859, lng: 7.5580 }
    map = L.map('map', {
        center: L.latLng(saintlouis),
        zoom: 13,
        zoomControl: false
    });
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png',
                { maxZoom: 18
                  , attribution: 'Open street map'
                }
               ).addTo(map)

    return map
}

