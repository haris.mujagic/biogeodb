const setbgregio = (name) => {
    const element = document.getElementById("bgregio")
    if (element) { element.textContent = name} 
}
function onEachFeature(feature, layer) {
    //bind click
    layer.on({
        click: (e) => {
            setbgregio(e.target.feature.properties.deregionna)
        }
    });
}
var addGeoJsonFeatureCollection = (fc) => {
    L.geoJSON(fc, {
        style: function(feature) {
            switch (feature.properties.regionnumm) {
            case 1: return {color: "cornflowerblue"}
            case 2: return {color: "blueviolet"}
            case 3: return {color: "darkorange"}
            case 4: return {color: "darkcyan"}
            case 5: return {color: "aquamarine"}
            case 6: return {color: "goldenrod"}
            default: return {color: "#000000"}
            }
        },
        onEachFeature: onEachFeature
        
    }).addTo(map)
}
