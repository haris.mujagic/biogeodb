let fundLayer
const getzip = async (popupElem, northing, easting) => {
    const response = await fetch(`https://api3.geo.admin.ch/rest/services/api/MapServer/identify?geometryType=esriGeometryPoint&geometry=${northing},${easting}&imageDisplay=0,0,0&mapExtent=0,0,0,0&tolerance=0&layers=all:ch.swisstopo.swissboundaries3d-gemeinde-flaeche.fill,ch.swisstopo-vd.ortschaftenverzeichnis_plz&returnGeometry=false`)
    let answer
    if (response.ok) {
        const json = await response.json()
        answer = json.results[0].attributes
        console.log(answer.gemname,)
        popupElem.querySelector("#zip").textContent = json.results[1].attributes.plz
        popupElem.querySelector("#municipality").textContent = json.results[0].attributes.gemname
        popupElem.querySelector("#canton").textContent = json.results[0].attributes.kanton
//        asyncHtml.innerHTML = `<p>Gemeinde: ${JSON.stringify(answer.gemname)}</p>
//            <p>ZIP: ${JSON.stringify(json.results[1].attributes.plz)}</p>`
//        popupElem.appendChild(asyncHtml)
        //document.getElementById("zip").textContent = json.results[1].attributes.plz
        //document.getElementById("municipality").textContent = json.results[0].attributes.gemname
        //document.getElementById("canton").textContent = json.results[0].attributes.kanton
    }
}
var showFundList = (mediath) => {
    if (mediath.length == 0){
        alert("no cases found")
        return 0
    }
    if (fundLayer) map.removeLayer(fundLayer)
    fundLayer = L.markerClusterGroup({ chunkedLoading: true });
    
    lonLatList=[]
    mediath.map((f) => {
        console.log(f)
        const lvch = Swisstopo.WGStoCH(f.latitude, f.longitude)
        var erstfinder = f.spezies.erstfinder;
        var art = f.spezies.art;
        var gattung = f.spezies.gattung;
        var title = gattung.concat(" ", art);// f.spezies.art
        var habitat = f.habitat
        var altitude = f.altitude
        var legit = f.legit
        var determinavit = f.determinavit
        var datumGefunden = f.datumGefunden
        var abundance = f.abundance
        var popup = () => {
            const newDiv = document.createElement("div")
            newDiv.innerHTML = `<p>${title}, ${erstfinder}</p>
            <p>Date: ${datumGefunden}</p>
            <p>Legit: ${legit}</p>
            <p>Determinavit: ${determinavit}</p>
            <p>Habitat: ${habitat}</p>
            <p>Altitude: ${altitude}</p>
            <p>Coordinates: ${f.latitude}, ${f.longitude}</p>
            <p>Abundance: ${abundance}</p>
            <p <span>ZIP: </span><span id="zip"></span>
            <p <span>Municipality: </span><span id="municipality"></span>
            <p <span>Canton: </span><span id="canton"></span>`
            
            getzip(newDiv, lvch[0],lvch[1])
            return newDiv
        }
        var marker = L.marker(L.latLng(f.latitude, f.longitude), {
            title: title.concat("  ", erstfinder)
        });
        marker.bindPopup(popup);
        fundLayer.addLayer(marker);
        lonLatList.push(L.latLng(f.latitude, f.longitude))
    })
    var bounds = new L.LatLngBounds(lonLatList).pad(0.5)
    map.fitBounds(bounds)

    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: 'Open street map' }).addTo(map)
    map.addLayer(fundLayer)
}

