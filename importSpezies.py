import csv
import re
from flaskApp.database import db_session
from flaskApp.fund.models import Spezies



with open('Systemat_Artenliste_Eur_2015.csv') as csvfile:
    spamreader = csv.reader(csvfile)
    regex = "[a-zäöüA-ZÄÖÜ]+\s[a-zäöüA-ZÄÖÜ]+"
    

    for row in spamreader:
        erstfinder = row[2]
        p = re.compile(regex)
        m = p.match(erstfinder)
        if m and m.group() != "Di Sabatino":
            print(row)
            p2 = re.compile(" ")
            erstfinder = "{}{}".format(
                p2.sub(", ", m.group()),
                erstfinder[m.end():]
            )
            print(erstfinder)
        spezi = Spezies(gattung=row[0], art=row[1], erstfinder=erstfinder)
        db_session.add(spezi)
        db_session.commit()

        # import pdb; pdb.set_trace()
        #print(', '.join(row))
