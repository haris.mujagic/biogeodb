from flask_wtf import FlaskForm
from wtforms import TextField, PasswordField, validators
from wtforms.fields.html5 import EmailField

class LoginForm(FlaskForm):
    email = EmailField(
        'Email address', [validators.DataRequired(), validators.Email()]
        )
    password = PasswordField(
        'Password', [validators.Required(), validators.Length(min=6, max=200)]
    )

class RegisterForm(FlaskForm):
    email = EmailField(
        'Email address', [validators.DataRequired(), validators.Email()]
        )
