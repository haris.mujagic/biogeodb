from sqlalchemy.sql.sqltypes import Boolean
from flask_login import UserMixin
from sqlalchemy import Column, Integer, String, Table, ForeignKey, Boolean
from sqlalchemy.orm import relationship
from werkzeug.security import generate_password_hash, check_password_hash
from ..database import Base
class Benutzende(UserMixin, Base):
    """Model for user accounts."""
    __tablename__ = 'benutzende'
    id = Column(Integer, primary_key=True)
    admin = Column(Boolean, nullable=False)
    active_member = Column(Boolean, nullable=False)
    email = Column(String(40), unique=True, nullable=False)
    password = Column(
        String(200),
        primary_key=False,
        unique=False,
        nullable=True
    )
    fundList = relationship(
        "Fund",
        secondary="benutzende_fund",
        back_populates='ownerList'
        )
    def __repr__(self):
        return '<User {}>'.format(self.email)
