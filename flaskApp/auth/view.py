from flask import app, current_app, Blueprint, abort
from flask import render_template, redirect, request, session, url_for
from flask.templating import render_template_string
from .model import Benutzende
from flask_login import login_user, logout_user
from flask_login import current_user as cu
from authlib.integrations.flask_client import OAuth
from ..database import db_session as db
from .serializer import serializeBenutzende
oauth = OAuth()

auth = Blueprint('auth', __name__,
                template_folder='templates', url_prefix='/auth')

@auth.route('/oidc_login')
def oidc_login():
    redirect_uri = url_for('auth.oidc_auth', _external=True)
    return oauth.gitlab.authorize_redirect(redirect_uri)

@auth.route('/oidc_callback')
def oidc_auth():
    try:
        token = oauth.gitlab.authorize_access_token()
        oidc_user = oauth.gitlab.userinfo()
    except Exception as e:
        return render_template(
            'unauthorized.html',
            message="OIDC authendikation failed: {}".format(e)
        )
    # user could also be added to session without need of flask_login
    # session['user'] = user
    try:
        user = Benutzende.query.filter_by(email=oidc_user.email).first()
    except Exception as e:
        return render_template(
            'unauthorized.html',
            message = ("Login user failed for oidc user with email {}, error: {}"
                       .format(oidc_user.email, e)
                       )
        )
    if user:
        try:
            login_user(user)
        except Exception as e:
            return render_template(
                'unauthorized.html',
                message = ("Login user failed for oidc user with email {}, error: {}"
                        .format(oidc_user.email, e)
                        )
            )
    else:
        return render_template(
            'unauthorized.html',
            message = ("Benutzer mit email {} hat keine Zugriffsrechte."
                    .format(oidc_user.email)
                    )
        )

    return redirect(url_for('auth.whoami'))

@auth.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')

    name = request.form['email']
    print(name)
    user = Benutzende.query.filter_by(email=name).first()

    if user is None:
        print('no user found with email: {}'.format(name))
    try:
        login_user(user)
    except:
        pass
    return redirect(url_for('auth.protected'))

@auth.route('/logout', methods=['GET'])
def logout():
    logout_user()
    return redirect(url_for('auth.protected'))

@auth.route('/register', methods=['GET', 'POST'])
def register():
    if (not cu.is_authenticated) or (cu.is_authenticated and not cu.admin):
        return render_template('unauthorized.html')
    if request.method == 'GET':
        return render_template('register.html')
    errors = []
    if request.method == 'POST':
        try:
            name = request.form['email']
            user = Benutzende(
                email = name,
                admin = False,
                active_member = True
            )
            db.add(user)
            db.commit()
            #print('Logged in as: {}'.format(flask_login.cu.name))
            print('Logged in as id: {}'.format(cu.id))
            print('Logged in as auth: {}'.format(cu.is_authenticated))
            return redirect(url_for('auth.protected'))

        except Exception as e:
            print('Error e:  {}'.format(e))
            errors.append(
                e
            )
            return redirect(url_for('auth.list_benutzende'))


@auth.route('/deactivate/<benutzende_id>', methods=['GET'])
def deactivate_benutzende(benutzende_id):
    if cu.is_authenticated and cu.admin:
        benutzendeDel = Benutzende.query.get(benutzende_id)
        benutzendeDel.active_member = False
        db.commit()
        print(benutzendeDel)
        return redirect(url_for('auth.list_benutzende'))
    else:
        return render_template('unauthorized.html')


@auth.route('/activate/<benutzende_id>', methods=['GET'])
def activate_benutzende(benutzende_id):
    if cu.is_authenticated and cu.admin:
        benutzendeDel = Benutzende.query.get(benutzende_id)
        benutzendeDel.active_member = True
        db.commit()
        print(benutzendeDel)
        return redirect(url_for('auth.list_benutzende'))
    else:
        return render_template('unauthorized.html')

@auth.route('/list', methods=['GET'])
def list_benutzende():
    if cu.is_authenticated and cu.admin:
        bList = [serializeBenutzende(b) for b in Benutzende.query.all()]
        return render_template(
            'benutzendeList.html',
            bList=bList
            )
    else:
        return render_template('unauthorized.html')


@auth.route('/whoami')
def whoami():
    if cu.is_authenticated:
        return render_template('whoami.html')
    return render_template('unauthorized.html')

@auth.route('/protected')
def protected():
    if cu.is_authenticated:
        return render_template('protected.html')
    return render_template('unauthorized.html')
