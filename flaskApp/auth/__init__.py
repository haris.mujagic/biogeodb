from .model import Benutzende
from flask import redirect, url_for, render_template
from flask_login import current_user, LoginManager, logout_user
login_manager = LoginManager()
login_manager.login_view = 'auth.login'

@login_manager.user_loader
def user_loader(userid):
    # TODO exception handling
    user = Benutzende.query.get(userid)
    if not user.active_member:
        print('{} is not active'.format(user.email))
        return None
    else:
        print('logged in as {}'.format(user))
        return user

# wenn in der view @login_required steht
#@login_manager.unauthorized_handler
#def unauthorized():
    # do stuff
    # return redirect(url_for('auth.login'))
    #return render_template('unauthorized.html')

# @login_manager.request_loader
# def request_loader(request):
#     name = request.form.get('name')
#     user = User.query.get(name)
#     if (user == None):
#         print('neinei:')
#         # print('neinei: {}'.format(current_user.is_authenticated))
#
#     else:
#         user.id = name
#         import pdb; pdb.set_trace()
#         print('JOJO: {} {}'.format(user.email, current_user.is_authenticated))
#         return user
#
#     return None
#
#     # DO NOT ever store passwords in plaintext and always compare password
#     # hashes using constant-time comparison!
#     # user.is_authenticated = request.form['password'] == users[name]['password']

