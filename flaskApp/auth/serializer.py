def serializeBenutzende(bObj):
    dataSet = {
        "email": bObj.email,
        "admin": bObj.admin,
        "id": bObj.id,
        "active_member": bObj.active_member
    }
    return dataSet
