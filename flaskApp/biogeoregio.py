import json
from sqlalchemy.sql import text
from .database import engine
from geoalchemy2 import shape


def getAsJson():
    with engine.connect() as con:
      fix = text("""
      SELECT json_build_object(
        'type', 'FeatureCollection',
        'features', json_agg(ST_AsGeoJSON(t2.*)::json)
      )
      FROM bgregio
      AS t2;
      """)
      rsFix = con.execute(fix)
      return rsFix.first()[0]
      # with open('dbf.json', 'w') as f:
      #     json.dump(featColl, f, indent=2)
      # return featColl
      
def getbgregio(lng,lat):
    with engine.connect() as con:
        fix = text("""
        SELECT deregionna, geom FROM bgregio
        WHERE ST_Contains(geom, ST_SetSRID(ST_Point({}, {}), 4326));
        """.format(lng,lat)
        )
        rsFix = con.execute(fix)
        if rsFix.rowcount > 0:
          return rsFix.first()[0]
        else:
          return "nicht gefunden"
          
