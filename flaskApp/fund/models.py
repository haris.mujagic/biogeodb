from tokenize import Double
from flask import Flask
from datetime import datetime
from sqlalchemy import Column, DateTime, Float, ForeignKey, Integer, String, Text, UniqueConstraint, Table
from sqlalchemy.orm import relationship, validates
from sqlalchemy.sql.functions import func
from sqlalchemy.schema import CheckConstraint
from geoalchemy2 import Geometry
from ..database import Base

def preventShortStrings (target, length):
    if len(target) <= length:
        raise ValueError('Legit name is too short, use more than two letters.')
    return target

class Spezies(Base):
    __tablename__ = 'spezies'
    id = Column(Integer, primary_key=True)
    gattung = Column(String, nullable=False)
    art = Column(String, nullable=False)
    UniqueConstraint('gattung', 'art', name='spezies')
    erstfinder = Column(String, nullable=True, default="unknown")
    fundList = relationship("Fund", backref='spezies', foreign_keys='Fund.spezies_id', lazy='dynamic')

class Fund(Base):
    __tablename__ = 'fund'
    id = Column(Integer, primary_key=True)
    spezies_id = Column(Integer, ForeignKey('spezies.id'), nullable=False)
    determinavit = Column(String)
    legit = Column(String, nullable=False)
    ###ownerList = relationship("User", backref='owner', foreign_keys='User.owner_id', lazy='dynamic')
    ownerList = relationship(
        "Benutzende",
        secondary="benutzende_fund",
        back_populates='fundList'
        )

    datumKreiert = Column(DateTime, nullable=False,
                             default=datetime.utcnow,
                             server_default=func.now()
    )
    datumGefunden = Column(DateTime, nullable=False,
                             default=datetime.utcnow,
                             server_default=func.now()
    )
    ort = Column(Geometry('POINT'), nullable=False)
    habitat = Column(String)
    abundance = Column(String)
    collectionId = Column(String)
    publication = Column(Text)
    altitude = Column(Float)    

    __table_args__ = (
        CheckConstraint('char_length(determinavit) > 2',
                        name='determinavit_min_length'),
    )
    @validates('determinavit')
    def validate_determinavit(self, key, det) -> str:
        return preventShortStrings (det, 2)

    __table_args__ = (
        CheckConstraint('char_length(legit) > 2',
                        name='legit_min_length'),
    )
    @validates('legit')
    def validate_legit(self, key, legit) -> str:
        return preventShortStrings (legit, 2)

benutzende_fund = Table(
    'benutzende_fund', Base.metadata,
    Column('fund_id', Integer, ForeignKey('fund.id')),
    Column('benutzende_id', Integer, ForeignKey('benutzende.id'))
)
