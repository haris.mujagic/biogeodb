from datetime import datetime
from flask_wtf import FlaskForm
from wtforms import IntegerField, DecimalField, FloatField, StringField, SelectField, TextAreaField
from wtforms.validators import DataRequired, InputRequired
from wtforms import validators
from wtforms.widgets import Select
from wtforms.fields import DateField


from .models import Spezies
class FundForm(FlaskForm):
    datumGefunden = DateField(default=datetime.now(), format='%Y-%m-%d', validators=[InputRequired()])
    longitude = DecimalField(places=7, validators=[DataRequired()])
    latitude = DecimalField(places=7, validators=[DataRequired()])
    altitude = DecimalField(places=1, validators=[DataRequired()])
    habitat = StringField('habitat')
    abundance = StringField('abundance')
    collectionId = StringField('collectionId')
    publication = TextAreaField('publication')
    legit = StringField('legit')
    determinavit = StringField('determinavit')
    speziesList = Spezies.query.all()
    spezies_id = SelectField(
        'Spezies',
        coerce=int,
        choices=[(spezies.id, "{} {} ({})".format(spezies.gattung, spezies.art, spezies.erstfinder)) for spezies in speziesList]
        )

    # autocomplete_input = StringField('autocomplete_input', validators=[DataRequired()])

class SpeziesForm(FlaskForm):
    art = StringField('art', validators=[DataRequired()])
    gattung = StringField('gattung', validators=[DataRequired()])

