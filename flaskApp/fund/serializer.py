import json
from ..biogeoregio import getbgregio
from geoalchemy2 import shape
def serializeFund(fundObj):
    lng = shape.to_shape(fundObj.ort).x
    lat = shape.to_shape(fundObj.ort).y
    dataSet = {
        "id": fundObj.id,
        "ownerList": [b.email for b in fundObj.ownerList],
        "datumKreiert": '{:%Y-%m-%d %H:%M}'.format(fundObj.datumKreiert),
        "datumGefunden": fundObj.datumGefunden.strftime("%Y-%m-%d"),
        "longitude": lng,
        "latitude": lat,
        "altitude": fundObj.altitude,
        "habitat": fundObj.habitat,
        "abundance": fundObj.abundance,
        "collectionId": fundObj.collectionId,
        "publication": fundObj.publication,
        "spezies": {
            "erstfinder": fundObj.spezies.erstfinder,
            "id": fundObj.spezies.id,
            "art": fundObj.spezies.art,
            "gattung": fundObj.spezies.gattung,
            "fundcount": len(fundObj.spezies.fundList.all())
        },
        "legit": fundObj.legit,
        "determinavit": fundObj.determinavit,
        "biogeoregio": getbgregio(lng,lat)
    }
    return dataSet
class FundForForm(object):
    def __init__(self, fundObj):
        self.longitude = shape.to_shape(fundObj.ort).x
        self.latitude = shape.to_shape(fundObj.ort).y
        self.id = fundObj.id
        self.datumKreiert = fundObj.datumKreiert
        self.datumGefunden = fundObj.datumGefunden
        self.altitude = fundObj.altitude
        self.habitat = fundObj.habitat
        self.abundance = fundObj.abundance
        self.collectionId = fundObj.collectionId
        self.publication = fundObj.publication
        self.spezies_id = fundObj.spezies_id
        self.determinavit_id = fundObj.determinavit_id
        self.legit_id = fundObj.legit_id