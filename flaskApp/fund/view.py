from sqlalchemy.sql.expression import false
import flask_login
from flask import jsonify
from flask import render_template, request, redirect, url_for
from flask_login import login_required, current_user
from .forms import FundForm
from .models import Fund, Spezies
from ..database import db_session as db
from .. import biogeoregio
from .serializer import serializeFund, FundForForm
from flask import Blueprint, render_template, abort
from jinja2 import TemplateNotFound
from ..auth.model import Benutzende


fund = Blueprint('fund', __name__,
                        template_folder='templates', url_prefix='/fund')

@fund.route('/', methods=['GET'])
def showFund():
    fundList = Fund.query.all()
    spezies = Spezies.query.all()
    legitList = list(set([f.legit for f in fundList]))
    speziesFiltered = [s for s in spezies if len(s.fundList.all()) > 0]
    return render_template(
        'map.html',
        fundList=[serializeFund(f) for f in fundList],
        biogeoregio=biogeoregio.getAsJson(),
        spezies=speziesFiltered,
        legitList=legitList,
        )


@fund.route('/list', methods=['GET'])
@login_required
def listFund():
    spezies = Spezies.query.all() 
    errors = []
    userFundList = []
    if current_user.admin:
        print("bla  pp")
        userFundList = Fund.query.all()
    else:
        print("blupp")
        userFundList = [
            f for f in Fund.query.all()
            if current_user in f.ownerList
            ]
        
    legitList = list(set([f.legit for f in userFundList]))
    fundList=[serializeFund(f) for f in userFundList]
    ownerList = [f for f in Benutzende.query.all()]
    return render_template(
        'list.html',
        fundList = sorted(fundList, key=lambda k: k['datumKreiert']),
        legitList = legitList,
        ownerList = ownerList,
        errors=errors,
        spezies = Spezies.query.all()
        )

@fund.route('/create', methods=['GET','POST'])
@login_required
def addFund():
    print(current_user)
    print(current_user.is_authenticated)
    errors = []
    speziesList = Spezies.query.all()
    form = FundForm(request.form)

    if not form.validate():
        errors.append(form.errors)
        errors.append(form.spezies_id.data)
        # get url that the user has entered
    try:
        fund = Fund(
            spezies_id=request.form['spezies_id'],
            legit=request.form['legit'],
            ort='POINT({} {})'.format(
                request.form['longitude'],
                request.form['latitude']
            ),
            altitude=request.form['altitude'],
            determinavit=request.form['determinavit'],
            datumGefunden=request.form['datumGefunden'],
            habitat=request.form['habitat'],
            abundance=request.form['abundance'],
            collectionId=request.form['collection_id'],
            publication=request.form['publication'],
            ownerList=[current_user]
        )
        db.add(fund)
        db.commit()
    except Exception as e:
        errors.append(e)
        print(errors)

    return render_template(
        'create.html',
        biogeoregio=biogeoregio.getAsJson(),
        errors=errors,
        form=form,
        speziesList=speziesList
    )

@fund.route('/update/<fund_id>', methods=['GET', 'POST'])
@login_required
def change(fund_id):
    fund = Fund.query.get(fund_id)
    isOwner = current_user in fund.ownerList
    if isOwner or current_user.admin:
        pass
    else:
        return render_template('unauthorized.html')
    errors = []
    speziesList = Spezies.query.all()
    form = FundForm()
    if request.method == 'POST':
        if not form.validate():
            errors.append(form.errors)
            errors.append(form.spezies_id.data)
        fund.spezies_id = request.form['spezies_id']
        fund.legit = request.form['legit']
        fund.determinavit = request.form['determinavit']
        fund.datumGefunden = request.form['datum_gefunden']
        longitude = request.form['longitude']
        latitude = request.form['latitude']
        fund.altitude = request.form['altitude']
        fund.ort='POINT({} {})'.format(longitude, latitude),
        fund.habitat = request.form['habitat']
        fund.abundance = request.form['abundance']
        fund.collectionId = request.form['collection_id']
        fund.publication = request.form['publication']
        # get url that the user has entered
        try:
            db.commit()

        except Exception as e:
            errors.append(
                e
            )
        else: 
            return redirect(url_for('fund.listFund'))
    return render_template('update.html',
                           errors=errors,
                           speziesList=speziesList,
                           form=form,
                           fund=serializeFund(fund),
                           biogeoregio=biogeoregio.getAsJson())

@fund.route('delete/<fund_id>', methods=['GET', 'DELETE'])
@login_required
def delete_fund(fund_id):
    fund = Fund.query.filter(fund_id == Fund.id)
    db.delete(fund.first())
    db.commit()
    return redirect(url_for('fund.listFund'))

@fund.route('/bgregio', methods=['GET'])
@login_required
def getbg():
    args = request.args
    lng = args.get("lng")
    lat = args.get("lat")
    bgregio = biogeoregio.getbgregio(lng,lat)
    return(jsonify(bgregio))


    
