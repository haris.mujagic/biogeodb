import os
from flask import Flask, render_template, request, jsonify, redirect, Blueprint, abort
from flask_login import LoginManager
from .auth import login_manager
from .auth.view import oauth

index = Blueprint('app', __name__, template_folder='templates')

@index.route('/', methods=['GET', 'POST'])
def welcome():
    return render_template('index.html')

def create_app(config_filename):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        TESTING = False,
        CSRF_ENABLED = True,
        SECRET_KEY = os.environ['SECRET_KEY'],
        GITLAB_CLIENT_ID = os.environ['GITLAB_CLIENT_ID'],
        GITLAB_CLIENT_SECRET = os.environ['GITLAB_CLIENT_SECRET'],
    )
    with app.app_context():
        from .auth.view import auth
        from .fund.view import fund
        app.register_blueprint(fund)
        app.register_blueprint(auth)
        app.register_blueprint(index)
        login_manager.init_app(app)
        oauth.init_app(app)
        oauth.create_client('gitlab')
        oauth.register(
            'gitlab',
            server_metadata_url='https://gitlab.com/.well-known/openid-configuration',
            client_kwargs={'scope': 'openid profile email'},
        )
    return app
