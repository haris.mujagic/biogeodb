{pkgs}: {
  leaflet = pkgs.stdenv.mkDerivation rec {
    pname = "leaflet";
    version = "1.8.0";
    src = pkgs.fetchzip {
      url = "https://leafletjs-cdn.s3.amazonaws.com/content/${pname}/v${version}/leaflet.zip";
      sha256 = "sha256-RwTCn7mTFOssX8NwWHGDqQc+DcdJmQGTsj3Zeh8xlTA=";
      stripRoot = false;
    };
    installPhase =
      ''
      mkdir -p $out/${pname}
      cp -v  $src/leaflet.js $out/${pname}/
      cp -v  $src/leaflet.js.map $out/${pname}/
      cp -v  $src/leaflet.css $out/${pname}/
      cp -vr $src/images $out/${pname}/
      '';
  };
  llMarkerCluster = pkgs.stdenv.mkDerivation rec {
    pname = "Leaflet.markercluster";
    version = "1.4.1";
    src = pkgs.fetchzip {
      url = "https://github.com/Leaflet/${pname}/archive/v${version}.zip";
      sha256 = "sha256-cTbumlC2sLG+BHtNWyUb+V9HOnzgLidiKUPYsAgytBo=";
      stripRoot = false;
    };
    installPhase = ''
          mkdir -p $out/${pname}
          cp -v $src/${pname}-${version}/dist/leaflet.markercluster.js $out/${pname}/
          cp -v $src/${pname}-${version}/dist/leaflet.markercluster.js.map $out/${pname}/
          cp -v $src/${pname}-${version}/dist/MarkerCluster.css $out/${pname}/
          cp -v $src/${pname}-${version}/dist/MarkerCluster.Default.css $out/${pname}/
        '';
  };

}
